package main;

@SuppressWarnings("serial")
public class Saskaita implements java.io.Serializable, Comparable<Saskaita> {

	// Variables
	private int inOut; // Takes, or gives
	private int cityID; // City code
	private String date; // The date
	private String name; // City name
	private double sum; // The amount of money
	
	public Saskaita(){ // Public no argument constructor
	}
	
	// Overrides
	public String toString(){
		return String.format(getInOut() + "|" + getDate() + "|" + getCityID()  + "|" + getName() + "|" + getSum());
	}
	
	// Getters and Setters
	public int getCityID() {
		return cityID;
	}
	public void setCityID(int cityID) {
		this.cityID = cityID;
	}	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getInOut() {
		return inOut;
	}
	public void setInOut(int inOut) {
		this.inOut = inOut;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public double getSum() {
		return sum;
	}
	public void setSum(double sum) {
		this.sum = sum;
	}

	// Comparing
	public int compareTo(final Saskaita b) { // Compare name and inOut descending
		if(getName().equals(b.getName())){ // If names are equal
			return Integer.compare(getInOut(), b.getInOut());
		} else {
			return Variables.LTCollator.compare(getName(), b.getName());
		}
	}
}
