package main;

import java.io.IOException;
import java.util.ArrayList;

import fileloader.FileLoader;
import sorter.Sorter;

public class Startup {

	@SuppressWarnings("unchecked")
	public static void main(String[] args){
		
		FileLoader fl = new FileLoader(); // New FileLoader
		Results r = new Results(); // New Results
		Sorter sorter = new Sorter(); // New Sorter
		
		ArrayList<Miestas> cities = fl.loadCities("./duomenys.xls", 0); // Loading cities
		ArrayList<Saskaita> bills = fl.loadBills("./duomenys.xls", 1, cities); // Loading bills
		
		r.displayData("Miestai", cities); // Displaying cities 
		r.displayData("S�skaitos", bills); // Displaying bills

		ArrayList<Miestas> sortedCities = (ArrayList<Miestas>) cities.clone(); // Cloning cities 
		ArrayList<Saskaita> sortedBills = (ArrayList<Saskaita>) bills.clone(); // Cloning bills 
		
		sortedCities = sorter.mergeSort(sortedCities); // Sorting cities
		sortedBills = sorter.mergeSort(sortedBills); // Sorting bills
		
		r.displayData("Miestai(Surikiuoti)", sortedCities); // Displaying cities 
		r.displayData("S�skaitos(Surikiuoti)", sortedBills); // Displaying bills

		r.calculateData(cities, bills); // Calculating bills
		r.displayResults(cities); // Displaying Final Results
		
		System.out.println("");
        System.out.println("Spauskite betkok� klavi��...");
        try {
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
