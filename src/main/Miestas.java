package main;

@SuppressWarnings("serial")
public class Miestas implements java.io.Serializable, Comparable<Miestas>{

	// Variables
	private int id; // ID
	private double balance; // Money balance
	private String name; // City name
	private int population; // City population
		
	public Miestas(){ // Public no argument constructor
	}
	
    // Overrides
	public String toString(){
		return String.format(getId() + "|" + getName() + "|" + getPopulation());
	}

	// Getters and Setters
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPopulation() {
		return population;
	}
	public void setPopulation(int population) {
		this.population = population;
	}

	// Comparing
	public int compareTo(final Miestas c) { // Comparing by city name descending
		return Variables.LTCollator.compare(getName(), c.getName());
	}
		
}
