package main;

import java.util.ArrayList;

public class Results {
	
	// Display array list elements as string
	public <T> void displayData(String title, ArrayList<T> list){ 
		System.out.println("---------- " + title + " ----------");
		for(int i = 0; i < list.size(); i++)
			System.out.println(list.get(i));
	}
	
	// Calculate every city bills
	public void calculateData(ArrayList<Miestas> c, ArrayList<Saskaita> b){
		for(int i = 0; i < b.size(); i++){
			Miestas city = c.get(findCityById(b.get(i).getCityID(), c));
			if(b.get(i).getInOut() != 0){ // If giving money
				city.setBalance(city.getBalance() + b.get(i).getSum());
			} else {
				city.setBalance(city.getBalance() - b.get(i).getSum());
			}
			
		}
	}
	
	// Display final results
	public void displayResults(ArrayList<Miestas> c){
		System.out.println("------- Galutiniai rezultatai -------");
		for(int i = 0; i < c.size(); i++){
			Miestas city = c.get(i);
			System.out.println("id=" + city.getId() + "," + city.getName() + ",likutis=" + city.getBalance());
		}
	}
	
	// Find city by id
	public static int findCityById(int id, ArrayList<Miestas> cities){
		for(int i = 0; i < cities.size(); i++)
			if(cities.get(i).getId() == id) return i;
		return -1;
	}
	
}
