package fileloader;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import main.Miestas;
import main.Results;
import main.Saskaita;

public class FileLoader {

	// Weird ass excel loading LOL.
	
	public ArrayList<Miestas> loadCities(String PATH, int sheet){
		
		// Variables
		Workbook wb;
		ArrayList<Miestas> resultList = new ArrayList<Miestas>();
		File file = new File(PATH);
		int startLine = 2; // Values starts
		
		try {
			wb = Workbook.getWorkbook(file);
			Sheet s = wb.getSheet(sheet);
			int row = s.getRows();
			for(int i = startLine; i < row; i++){
				Miestas c = new Miestas();
				c.setId(Integer.parseInt(s.getCell(0, i).getContents())); // Setting id
				c.setName(s.getCell(1, i).getContents()); // Setting name
				c.setPopulation(Integer.parseInt(s.getCell(2, i).getContents())); // Setting population
				resultList.add(c);
			}
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return resultList;
	}
	public ArrayList<Saskaita> loadBills(String PATH, int sheet, ArrayList<Miestas> cities){
		
		// Variables
		Workbook wb;
		ArrayList<Saskaita> resultList = new ArrayList<Saskaita>();
		File file = new File(PATH);
		int startLine = 2, tmp; // Values starts
		
		try {
			wb = Workbook.getWorkbook(file);
			Sheet s = wb.getSheet(sheet);
			int row = s.getRows();
			for(int i = startLine; i < row; i++){
				Saskaita c = new Saskaita();
				c.setInOut(Integer.parseInt(s.getCell(0, i).getContents())); // Setting city id
				c.setDate(s.getCell(1, i).getContents()); // Setting date
				
				tmp = (Integer.parseInt(s.getCell(2, i).getContents())); 
				c.setCityID(tmp); // City ID
				c.setName(cities.get(Results.findCityById(tmp, cities)).getName());// Setting city name
				
				c.setSum(Double.parseDouble(s.getCell(3, i).getContents())); // Setting city code
				resultList.add(c);
			}
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return resultList;
	}
	
}
