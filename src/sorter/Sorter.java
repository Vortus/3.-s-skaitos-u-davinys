package sorter;

import java.util.ArrayList;
import java.util.Collections;

public class Sorter {

	// Simple insertion sort
	public <T extends Comparable<T>> void insertionSort(ArrayList<T> list){ 
		int key; // Variables
		for(int i = 0; i < list.size(); i++){ // Sorting
			key = i;
			while(key > 0 && list.get(key).compareTo(list.get(key - 1)) < 0){
				Collections.swap(list, key, key - 1); // Swapping elements
				key--;
			}
		}
	}
	
	/// Merge sort 
	private  <T extends Comparable<T>> void merge(ArrayList<T> left, ArrayList<T> right, ArrayList<T> result) {
	    int leftIndex = 0;
	    int rightIndex = 0;
	    int resultIndex = 0;

	    while (leftIndex < left.size() && rightIndex < right.size()) {
	        if ( (left.get(leftIndex).compareTo(right.get(rightIndex))) < 0) {
	            result.set(resultIndex, left.get(leftIndex));
	            leftIndex++;
	        } else {
	        	result.set(resultIndex, right.get(rightIndex));
	            rightIndex++;
	        }
	        resultIndex++;
	    }
	 
	    ArrayList<T> rest;
	    int restIndex;
	    if (leftIndex >= left.size()) {
	        rest = right;
	        restIndex = rightIndex;
	    } else {
	        rest = left;
	        restIndex = leftIndex;
	    }
	 
	    for (int i = restIndex; i < rest.size(); i++) {
	    	result.set(resultIndex, rest.get(i));
	        resultIndex++;
	    }
	}
	
	public <T extends Comparable<T>> ArrayList<T> mergeSort(ArrayList<T> result) {
	    ArrayList<T> left = new ArrayList<T>();
	    ArrayList<T> right = new ArrayList<T>();
	    int center;
	 
	    if (result.size() == 1) {    
	        return result;
	    } else {
	        center = result.size()/2;
	        for (int i = 0; i < center; i++) {
	                left.add(result.get(i));
	        }
	 
	        for (int i = center; i < result.size(); i++) {
	                right.add(result.get(i));
	        }
	 
	        left  = mergeSort(left);
	        right = mergeSort(right);
	 
	        merge(left, right, result);
	    }
	    return result;
	}
     
	
}
